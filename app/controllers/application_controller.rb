include SessionsHelper

class ApplicationController < ActionController::Base
  before_action :load_settings

  private
  def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
  end

  def load_settings
    @settings = session[:settings]
    @settings ||= { 'xssStore' => true, 'xssRender' => true, 'accessControl' => true }
  end
  
  def check_if_true(item)
    ActiveModel::Type::Boolean.new.cast(item)
  end
end
