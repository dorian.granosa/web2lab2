class MessagesController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :authorize, only: [:edit, :update, :destroy]

  def home
  end

  def save
    @settings['xssStore'] = settings_params[:xssStore]
    @settings['xssRender'] = settings_params[:xssRender]
    @settings['accessControl'] = settings_params[:accessControl]
    
    session[:settings] = @settings

    flash[:success] = "Saved!"
    redirect_to root_path
  end

  def new
    @message  = current_user.messages.new
  end

  def index
    @messages = Message.all
  end

  def create
    attributes = message_params.clone
    if check_if_true(@settings['xssStore'])
      sanitizer = Rails::Html::FullSanitizer.new
      attributes[:title] = sanitizer.sanitize(attributes[:title])
      attributes[:body] = sanitizer.sanitize(attributes[:body])
    end

    @message = current_user.messages.build(attributes)
    if @message.save
      flash[:success] = "Message has been created!"
      redirect_to @message
    else
      render 'new'
    end
  end

  def edit
    @message = Message.find(params[:id])
  end

  def update
    attributes = message_params.clone
    if check_if_true(@settings['xssStore'])
      sanitizer = Rails::Html::FullSanitizer.new
      attributes[:title] = sanitizer.sanitize(attributes[:title])
      attributes[:body] = sanitizer.sanitize(attributes[:body])
    end
    
    @message = Message.find(params[:id])
    if @message.update(attributes)
      flash[:success] = "Message updated"
      redirect_to @message
    else
      render 'edit'
    end
  end

  def destroy
    @message = Message.find(params[:id])

    if @message
      @message.destroy
      flash[:success] = "Message has been deleted"
    else
      flash[:danger] = "Error"
    end
    redirect_to messages_path
  end

  def show
    @message = Message.find(params[:id])
  end

  private

  def settings_params
    params.require(:settings).permit(:xssStore, :xssRender, :accessControl)
  end

  def message_params
    params.require(:message).permit(:title, :body)
  end

  def authorize
    load_settings
    message = Message.find(params[:id])

    if check_if_true(@settings['accessControl'])
      unless message.user == current_user || current_user.role == 'admin'
        flash[:danger] = "You don't have permission!"
        redirect_to messages_path
      end
    end
  end
end
