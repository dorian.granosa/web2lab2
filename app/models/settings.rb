class Settings
  include ActiveModel::Model
  attr_accessor :xssStore, :xssRender, :accessControl

  def initialize(attributes={})
    super
    @xssStore ||= true
    @xssRender ||= true
    @accessControl ||= true
  end
end