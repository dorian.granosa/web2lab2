class User < ApplicationRecord
  has_many :messages
  has_secure_password

  validates :name, presence: true
  validates :password, presence: true, length: { minimum: 6 }
end