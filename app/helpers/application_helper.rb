module ApplicationHelper
  def check_if_true(item)
    ActiveModel::Type::Boolean.new.cast(item)
  end

  def check_xss_render(item)
    if check_if_true(@settings['xssRender'])
      return item
    else
      return item.html_safe
    end
  end
end
