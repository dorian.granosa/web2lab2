module MessagesHelper
  def can_edit(message, user)
    message.user == user
  end

  def can_delete(message, user)
    return message.user == user || (user && user.role == 'admin')
  end
end
