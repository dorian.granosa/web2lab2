Rails.application.routes.draw do
  root 'messages#home'
  
  resources :users
  resources :messages

  post   '/',                    to: 'messages#save'

  get    '/messages/:id/delete', to: 'messages#destroy'
  
  get    '/signup',              to: 'users#new'
  get    '/login',               to: 'sessions#new'
  post   '/login',               to: 'sessions#create'
  get    '/logout',              to: 'sessions#destroy'
  delete '/logout',              to: 'sessions#destroy'
end
